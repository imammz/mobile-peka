import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailObsPageRoutingModule } from './detail-obs-routing.module';

import { DetailObsPage } from './detail-obs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailObsPageRoutingModule
  ],
  declarations: [DetailObsPage]
})
export class DetailObsPageModule {}
