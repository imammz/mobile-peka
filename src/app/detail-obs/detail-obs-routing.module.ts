import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailObsPage } from './detail-obs.page';

const routes: Routes = [
  {
    path: '',
    component: DetailObsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailObsPageRoutingModule {}
