import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailObsPage } from './detail-obs.page';

describe('DetailObsPage', () => {
  let component: DetailObsPage;
  let fixture: ComponentFixture<DetailObsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailObsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailObsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
