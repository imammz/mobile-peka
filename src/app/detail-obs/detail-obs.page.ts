import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ObservasiService } from '../services/observasi.service';
import { Storage } from '@ionic/storage';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-detail-obs',
  templateUrl: './detail-obs.page.html',
  styleUrls: ['./detail-obs.page.scss'],
})
export class DetailObsPage implements OnInit {

  obs: any = [];
  IDobservasion = '';
  userData: any = [];
  area = [];
  subarea = [];
  unsafe=[];
  unsafedetail=[];
  pegawai=[];
  CostCenter = [];
  unsafeDetailC = [];

  param = {
    IDklasifikasi:'',
    spesifikasi:'',
    DateObs:'',
    Pengamatan:'',
    Lanjutan:'',
    Klasifikasi:'',
    FilePhoto:'',
    IDNIK:'',
    NamaEmploye:'',
    FungsiName:'',
    Email:'',
    NoTlp:'',
    CreateID:'',
    AreaID:'',
    jabatan:'',
    processApl:0,
    langsung:'',
    unsafeDetailId:'',
    CostCenter:'',
    UnsafeID:'',
    lokasi_tempat:'',
    processApl_desc: '',
    IDobservasion: '',
    IsActive: null,
    CreateDate: '',
    PICNIK: null,
    PICSign: null,
    PISignDate: '',
    PICEmail: null,
    PICInformasi: null,
    RiskA: null,
    RiskB: null,
    RejectReason: null,
    Pengelolahinfor: null,
    UserBypass: null,
    BypassDate: '',
    Aksi: null,
    AksiDate: '',
    AksiComment: null,
  };

  constructor(private ObservasiProvider: ObservasiService,
    private storage: Storage,
    private router: Router,
    private routeAct: ActivatedRoute,
    public global: GlobalService) { }

  ngOnInit() {

    this.IDobservasion = this.routeAct.snapshot.paramMap.get("id");
    console.log(this.IDobservasion);
   
    this.router.navigate(['/edit-obs/'+this.IDobservasion+'/1']);
    
/*
    this.listArea();
    this.listUnsafe();
    this.listEmploye();
    this.listPostCenter();

     setTimeout(() => { 
      this.getObsById();
    }, 500);

    this.storage.get('userData').then((val) => {
      this.userData = val;
    });
   */
  }




  getObsById(): void { 
    this.global.showLoader();
    var data = {IDobservasion: this.IDobservasion};
    
    this.ObservasiProvider.get(data).subscribe( ress =>
        {
            this.param = ress.data[0];
            this.global.hideLoader();
            console.log(this.param); 



        }
      );
  }


  listArea(): void { 
  
       this.ObservasiProvider.listArea().subscribe( ress =>
         {
             this.area = ress.data;
             console.log(this.area);        
         }
       );
       
 
   }
 
 
   listSubArea(idk): void { 
     console.log(idk);
     var ID = idk;
 
     this.subarea = [];
     this.param.AreaID = '';
 
     this.global.showLoader();
     this.ObservasiProvider.listSubArea({KlarifikasiID:ID}).subscribe( ress =>
         {
             this.subarea = ress.data;
             this.global.hideLoader();
             console.log(this.subarea);
 
         }
       );
   }
 
 
   listUnsafe(): void { 
    // this.global.showLoader();
 
     this.ObservasiProvider.listUnsafe().subscribe( ress =>
         {
             this.unsafe = ress.data;
             
            //  this.global.hideLoader();
             console.log(this.unsafe);
             console.log(this.unsafeDetailC);
         }
       );
   }
 
 

   listEmploye(): void { 
    // this.global.showLoader();
     this.ObservasiProvider.listEmployee().subscribe( ress =>
         {
             this.pegawai = ress.data;
            //  this.global.hideLoader();
             console.log(this.pegawai);
         }
       );
   }
 
 
   listPostCenter(): void { 
     // this.global.showLoader();
      this.ObservasiProvider.listPostCenter().subscribe( ress =>
          {
              this.CostCenter = ress.data;
             //  this.global.hideLoader();
              console.log(this.CostCenter);
          }
        );
    }
 


}
