import { Iemploye } from './../interfaces/iemploye';
import { Directive, HostBinding, ElementRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { GlobalService } from '../services/global.service';
import { ObservasiService } from '../services/observasi.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {

login = '';
passwordCheckbox = false; 
user: any = [];
userData: any = [];
userLdap: any = [];
      

  constructor(public router: Router,
    private storage: Storage,
    private global: GlobalService,
    private obs: ObservasiService) { 
  }

  ngOnInit(): void {

    // set a key/value
  
    this.storage.get('userData').then((val) => {
      this.userData = val;
      console.log(this.userData);
      
      console.log(this.userData.UserName);    

    });


    this.storage.get('login').then((val) => { 
      if(val == true ) {
      //  this.router.navigate(['/home']);

      }
  });

  }


  ionViewDidEnter() {
    console.log('Items did enter');
    this.global.hideLoader();
  }


  showPassword(input: any): any {
    input.type = input.type === 'password' ?  'text' : 'password';
   }


   prosesLogin() {

    this.global.showLoaderLong();
   // console.log(this.user);
    this.obs.login({UserName:this.user.UserName, Password: this.user.Password}).subscribe( ress =>
      {
          this.userData = ress.data;
          this.userLdap = ress.ldap_data;
         
          this.global.hideLoader();
          

          if(ress.value == true) {

            let getUser = this.userData[0];
            getUser.CostCenter = getUser.CostCenter.trim();

            console.log(getUser);
            console.log(this.userLdap);
            this.storage.set('login', true); 
            this.storage.set('userData', getUser); 
            this.storage.set('userLdap', this.userLdap); 
          

            localStorage.setItem('userData',getUser);
            localStorage.setItem('userLdap',this.userLdap);
            localStorage.setItem('login','true');

            this.router.navigate(['/redirect']);
   
          }
          else {
              console.log(ress);
              this.global.presentAlert('Alert','Gagal Login','Username atau Password Anda Salah')
          }
          


      }
    );
   }


   prosesLoginX() {

    this.global.showLoader();

    this.obs.login({UserName:this.user.UserName, Password: this.user.Password}).subscribe( ress =>
      {
         /*
          this.userData.UserName = 'jodhi.sugihartono';
          this.userData.OlahID = 1;
          this.userData.PICID = 1;
          this.userData.Nama = 'jodhi sugihartono';
          this.userData.Email = 'jodhi.sugihartono@gmail.com';
          this.userLdap = [];
         */

          this.global.hideLoader();

          if(true) {
            console.log(this.userData);
            console.log(this.userLdap);
            this.storage.set('login', true); 
            this.storage.set('userData', this.userData); 
            this.storage.set('userLdap', this.userLdap); 
            this.router.navigate(['/redirect']);
          }
          else {
              console.log(ress);
              this.global.presentAlert('Alert','Gagal Login','Username atau Password Anda Salah')
          }
          


      }
    );
   }

}
