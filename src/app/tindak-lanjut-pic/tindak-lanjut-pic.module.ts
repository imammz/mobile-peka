import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TindakLanjutPicPageRoutingModule } from './tindak-lanjut-pic-routing.module';

import { TindakLanjutPicPage } from './tindak-lanjut-pic.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TindakLanjutPicPageRoutingModule
  ],
  declarations: [TindakLanjutPicPage]
})
export class TindakLanjutPicPageModule {}
