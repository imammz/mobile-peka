import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TindakLanjutPicPage } from './tindak-lanjut-pic.page';

describe('TindakLanjutPicPage', () => {
  let component: TindakLanjutPicPage;
  let fixture: ComponentFixture<TindakLanjutPicPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TindakLanjutPicPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TindakLanjutPicPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
