import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TindakLanjutPicPage } from './tindak-lanjut-pic.page';

const routes: Routes = [
  {
    path: '',
    component: TindakLanjutPicPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TindakLanjutPicPageRoutingModule {}
