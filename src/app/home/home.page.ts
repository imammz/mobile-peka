import { ObservasiService } from './../services/observasi.service';
import { Component, ElementRef, HostListener  } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { GlobalService } from '../services/global.service';
import { Platform } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  obs = [];
  obsDummy = [];
  userData: any = [];
  segment = 0;
  listSubmited = "0";

  constructor(private ObservasiProvider: ObservasiService,
   public global: GlobalService,
    private storage: Storage,
    private router: Router,
    private elementRef: ElementRef,
    private platform: Platform
    ) {


    
  }


  segmentChanged(ev: any) {
    console.log('', ev.detail.value);
    console.log('segment : ', this.segment);
    this.getObs(this.userData.UserName,ev.detail.value,this.userData.CostCenter);
  }

  

  ngOnInit(): void {
   
    /*
    this.storage.get('userData').then((val) => {
      this.userData = val;
      console.log(this.userData);   
      console.log(this.userData.UserName);    
      console.log(this.userData.Password);    
      console.log(this.userData.CostCenter);    
      this.getObs(this.userData.UserName,this.segment,this.userData.CostCenter);
    });
    
   */   
  
  }
  

  @HostListener('unloaded')
  ngOnDestroy() {
    console.log('Items destroyed');
  }

  ionViewDidEnter() {
    console.log('Items did enter');
    
    setTimeout(() => { 
      this.storage.get('userData').then((val) => {
        this.userData = val;
        console.log(this.userData);   
        console.log(this.userData.UserName);    
        console.log(this.userData.Password);    
        console.log(this.userData.CostCenter);    
        this.getObs(this.userData.UserName,this.segment,this.userData.CostCenter);
        this.userData.CostCenter = this.userData.CostCenter.trim(); 
      });
      }, 100);
   


  }

  getInitData(): void {
    this.userData = localStorage.getItem('userData');
    this.getObs(this.userData.UserName,this.segment,this.userData.CostCenter);
    console.log(localStorage.getItem('userData'));

  }

  getObs(username, filter, costcenter): void { 
    this.obs = [];
    console.log('segment : ', this.segment);
    this.global.showLoaderShort();
    var data = {UserName: username,
                filter:filter,costcenter:costcenter};
    
    this.ObservasiProvider.list(data).subscribe( ress =>
        {   
          ress.data.forEach(el => {
           
           if(el.PICSign == null) {
            el.PICSign = ''; 
           }
           
             this.obs.push(el);
         });


            this.global.hideLoader();
            console.log(this.obs);

        }
      );

      
  }



  ngOndestroy() {
    this.elementRef.nativeElement.remove();
  }







}

  


