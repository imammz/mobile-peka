import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SetPicObsPageRoutingModule } from './set-pic-obs-routing.module';

import { SetPicObsPage } from './set-pic-obs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SetPicObsPageRoutingModule
  ],
  declarations: [SetPicObsPage]
})
export class SetPicObsPageModule {}
