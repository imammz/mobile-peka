import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SetPicObsPage } from './set-pic-obs.page';

describe('SetPicObsPage', () => {
  let component: SetPicObsPage;
  let fixture: ComponentFixture<SetPicObsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetPicObsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SetPicObsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
