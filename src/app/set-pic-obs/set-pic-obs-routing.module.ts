import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SetPicObsPage } from './set-pic-obs.page';

const routes: Routes = [
  {
    path: '',
    component: SetPicObsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SetPicObsPageRoutingModule {}
