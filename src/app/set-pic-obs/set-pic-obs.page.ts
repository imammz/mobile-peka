import { Component, OnInit } from '@angular/core';
import { ObservasiService } from '../services/observasi.service';
import { LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from '../services/global.service';
import { Storage } from '@ionic/storage';

const today = new Date();
@Component({
  selector: 'app-set-pic-obs',
  templateUrl: './set-pic-obs.page.html',
  styleUrls: ['./set-pic-obs.page.scss'],
})
export class SetPicObsPage implements OnInit {

  uniqueTime = new Date().getTime();
  

  loader = null;
  KlarifikasiID = '';
  UnsafeID= '';
  userData: any = [];
  error = 0;
  msg = '';
  IDobservasion = '';
  opsi = "0";
  
  AreaID = '';

  area = [];
  subarea = [];
  unsafe=[];
  unsafedetail=[];
  pegawai=[];
  CostCenter = [];
  jenisPekerja = 'pekerja';

  unsafedetailVal = [];
  processApl = 0;
  indexunsafe = 0;

  unsafeDetailC = [];

  slideimg = [];


  param = {
    IDklasifikasi:'',
    spesifikasi:'',
    DateObs:'',
    Pengamatan:'',
    Lanjutan:'',
    Klasifikasi:'',
    FilePhoto:'',
    IDNIK:'',
    NamaEmploye:'',
    FungsiName:'',
    Email:'',
    NoTlp:'',
    CreateID:'',
    AreaID:'',
    jabatan:'',
    processApl:0,
    langsung:'',
    unsafeDetailId:'',
    CostCenter:'',
    UnsafeID:'',
    lokasi_tempat:'',
    processApl_desc: '',
    IDobservasion: '',
    IsActive: null,
    CreateDate: '',
    PICNIK: null,
    PICSign: null,
    PISignDate: today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate(),
    PICEmail: null,
    PICInformasi: null,
    RiskA: null,
    RiskB: null,
    RejectReason: null,
    Pengelolahinfor: null,
    UserBypass: null,
    BypassDate: '',
    Aksi: null,
    AksiDate: '',
    AksiComment: null,
    unsafedetail: [],
    costcenterlist: [],
    arealist: [],
    subarealist: [],
    pekaemployeelist: [],
    unsafelist: []
  };

  setPIC = true;

  probability =[
  {key:'A',val:'Tidak pernah terdengan terdengar di industri hulu' },    
  {key:'B',val:'Terdengar di industri hulu migas'},    
  {key:'C',val:'Pernah terjadi di sebuah industri migas di Indonesia' },    
  {key:'D',val:'Terjadi beberapa kali pertahun di sebuah industri migas di indonesia' },    
  {key:'E',val:'Terjadi beberapa kali pertahun di tempat kerja disalah satu perusahaan' },    
  ];

  konsekuensi = [
    {key:0, manusia:'Tidak ada dampak kesehatan / kecelakaan', alat:'Tidak ada kerusakan ', lingkungan: 'Tidak ada dampak', citra: 'Tidak ada pengaruh'},
    {key:1, manusia:'Dampak kesehatan/kecelakaan sangat kecil', alat:'Kerusakan sangat kecil', lingkungan: 'Dampak sangat kecil', citra: 'Pengaruh Kecil'},
    {key:2, manusia:'Dampak kesehatan / kecelakaan kecil', alat:'Kerusakan kecil', lingkungan: 'Dampak kecil', citra: 'Pengaruh terbatas'},
    {key:3, manusia:'Dampak kesehatan / kecelakaan utama', alat:'Kerusakan yang terbatas', lingkungan: 'Dampak yang terbatas', citra: 'Dampak yang terbatas'},
    {key:4, manusia:'Fatalitas Tunggal', alat:'Kerusakan Utama', lingkungan: 'Dampak utama', citra: 'Pengaruh Nasional'},
    {key:5, manusia:'Fatalitas Ganda', alat:'Kerusakan yang luas', lingkungan: 'Dampak besar', citra: 'Pengaruh Internasional'}
  ]


   matrix = {
     probability: '',
     konsekuensi: 0
   }

  constructor( private ObservasiProvider: ObservasiService,
    public loadingController: LoadingController,
    private router: Router,
    public global: GlobalService,
    private routeAct: ActivatedRoute,
    private storage: Storage) { }

  ngOnInit() {

    this.IDobservasion = this.routeAct.snapshot.paramMap.get("id");
    this.opsi = '1';
    this.setPIC = false;
    
    console.log(this.IDobservasion);


    this.storage.get('userData').then((val) => {
      this.userData = val;
      this.param.CreateID    = this.userData.UserName;
      this.param.Email    = this.userData.Email;
      this.param.PICEmail    = this.userData.Email;
      this.param.PISignDate = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    });



  }

  ionViewDidEnter() {
   

    this.getObsById();
   // this.listUnsafe();

   setTimeout(() => { 
    this.slideimg = [];
    console.log('img 1'); 
    this.param.FilePhoto.split(',').forEach(el => {
      if(el!='-' && el!='') {
        this.slideimg.push(el);
      
        console.log(el); 
      };
   });  
   
    },3500);


   setTimeout(() => { 
    this.slideimg = [];
    console.log('img 2');
    this.param.FilePhoto.split(',').forEach(el => {
      if(el!='-' && el!='') {
        this.slideimg.push(el);
      
        console.log(el); 
      };
   });  
  
    },6500);
  
    this.storage.get('userData').then((val) => {
      this.userData = val;
      this.param.CreateID    = this.userData.UserName;
      this.param.Email    = this.userData.Email;
      this.param.PICEmail    = this.userData.Email;
      this.param.PISignDate = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    });


    console.log(this.param.PICEmail);
    console.log('CC : '+this.userData.CostCenter);
  }


  checkNearMiss(id): void {
   
    if(id == 41) {
    //  alert(this.unsafeDetailC[41]);
      //this.unsafeDetailC[41] = true;
      this.unsafeDetailC[42] = false;

    }
    else if(id == 42) {
     // alert(this.unsafeDetailC[42]);
      this.unsafeDetailC[41] = false;
      //this.unsafeDetailC[42] = true;

      
    }

  }

  getMatrix(): void { 
   
  }


  
  getObsById(): void { 
    this.global.showLoader();
    var data = {IDobservasion: this.IDobservasion};
    this.slideimg = [];
    this.ObservasiProvider.get(data).subscribe( ress =>
        {
            this.param = ress.data[0];
            this.global.hideLoader();
            console.log(this.param); 



           setTimeout(() => { 
            this.listUnsafeDetail(this.param.UnsafeID);
            },1500);

            if(this.param.subarealist.length == 0) {
              this.listSubArea(this.param.IDklasifikasi);
              setTimeout(() => { 
                this.param.AreaID = this.AreaID; 
                },2500);
            }

            this.param.UnsafeID = this.param.Klasifikasi;
            this.AreaID = this.param.AreaID;
            this.param.PISignDate = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();

            this.area = this.param.arealist;
            this.subarea = this.param.subarealist;
            this.CostCenter = this.param.costcenterlist;
            this.pegawai = this.param.pekaemployeelist;
            this.unsafe = this.param.unsafelist;

            this.param.PICSign = this.param.PICSign.trim(); 
            this.userData.CostCenter = this.userData.CostCenter.trim(); 

          

         

                       
        


         
          


        }
      );
  }


  listArea(): void { 
   // this.global.showLoader();


      this.ObservasiProvider.listArea().subscribe( ress =>
        {
            this.area = ress.data;
           //  this.global.hideLoader();
          
      //     this.getObsById();
            console.log(this.area);
           
          
            setTimeout(() => { 
              console.log(this.param.IDklasifikasi);        
        //      this.listSubArea(this.param.IDklasifikasi,1);
            },1800);
                     

        }
      );
      

  }


  listSubArea(idk,reload=0): void { 
    console.log(idk);
    var ID = idk;

    this.subarea = [];
    this.param.AreaID = '';

    this.global.showLoader();
    this.ObservasiProvider.listSubArea({KlarifikasiID:ID}).subscribe( ress =>
        {
            this.subarea = ress.data;
            this.global.hideLoader();
            console.log(this.subarea);

            this.param.AreaID = this.AreaID; 
          //  this.getObsById();
        

            

        }
      );
  }


  listUnsafe(): void { 
   // this.global.showLoader();

    this.ObservasiProvider.listUnsafe().subscribe( ress =>
        {
            this.unsafe = ress.data;
            
           //  this.global.hideLoader();
            console.log(this.unsafe);
            console.log(this.unsafeDetailC);
            console.log('unsafeid : '+this.param.UnsafeID);
            console.log('klasifikasi : '+this.param.Klasifikasi);

        


        }
      );
  }


  
  listUnsafeDetail(idk): void { 

    console.log(idk);
    var ID = idk;
 
    this.unsafedetail = [];
   // this.param.unsafeDetailId = '';

   // this.global.showLoader();
    this.ObservasiProvider.listUnsDetail({UnsafeID:ID}).subscribe( ress =>
        {
            this.unsafedetail = ress.data;
        //     this.global.hideLoader();
           // console.log(Object.entries(ress.data));
            console.log(this.unsafedetail);

      
    for (let [key, value] of Object.entries(ress.data)) {
       
     
      for (let [key2, value2] of Object.entries(value)) {
        var getVal: any = value2;
        
       
        for (let [key3, value3] of Object.entries(this.param.unsafedetail)) { 

          if(value3.Subksid == getVal.UnsDetail ) {
            this.unsafeDetailC[getVal.UnsDetail] = true;

          }

        }
      
      }


      
    }
            

        }
      );
  }


  listPostCenter(): void { 
    // this.global.showLoader();
     this.ObservasiProvider.listPostCenter().subscribe( ress =>
         {
             this.CostCenter = ress.data;
            //  this.global.hideLoader();
             console.log(this.CostCenter);
         }
       );
   }


   simpan(proses): void{

    this.param.processApl = proses;
    this.msg = '';

    var tempUnsafedetail = [];
    var i= 0;
    for (let [key, value] of Object.entries(this.unsafeDetailC)) {
    
      if(value == true) {
        tempUnsafedetail[i] = key;
      }
       i++;
    }

    this.param.unsafeDetailId = tempUnsafedetail.toString();
    this.param.Klasifikasi = this.param.UnsafeID;
    
   console.log(this.param.unsafeDetailId);

    this.param.RiskA = this.matrix.probability;
    this.param.RiskB = this.matrix.konsekuensi;
    this.param.PICEmail    = this.userData.Email;

    if(this.param.processApl == 400){
      if(this.param.RejectReason == null) {
        this.error = 1;
        this.msg += 'Komentar Approval Tidak Boleh Kosong; \n';
       }
       else {
        this.error = 0;
       }
    }

    if(this.param.processApl == 900){
      if(this.param.RejectReason == null) {
        this.error = 1;
        this.msg += 'Reason Reject Tidak Boleh Kosong; \n';
       }
       else {
        this.error = 0;
       }
    }

    


       if(this.setPIC == true) {
        if(this.param.BypassDate == undefined) {
          this.error = 1;
          this.msg += 'Batas Akhir Tindak Lanjut Harus Diisi; \n \n';
         }
         else {
          this.error = 0;
         }


          if( this.matrix.probability == undefined) {
        this.error = 1;
        this.msg += 'Matrix Tingkat Resiko -Probability Tidak Boleh Kosong; \n \n';
       }
       else {
        this.error = 0;
       }


      if( this.matrix.probability == undefined) {
        this.error = 1;
        this.msg += 'Matrix Tingkat Resiko - Konsekuensi terhadap objek Tidak Boleh Kosong; \n \n';
       }
       else {
        this.error = 0;
       }
       }
       else {
        if( this.matrix.probability == undefined) {
          this.error = 1;
          this.msg += 'Matrix Tingkat Resiko -Probability Tidak Boleh Kosong; \n \n';
         }
         else {
          this.error = 0;
         }
  
  
        if( this.matrix.probability == undefined) {
          this.error = 1;
          this.msg += 'Matrix Tingkat Resiko - Konsekuensi terhadap objek Tidak Boleh Kosong; \n \n';
         }
         else {
          this.error = 0;
         }
       }




    if(this.error == 1) {
      this.global.presentAlert('Error','Gagal Proses',this.msg);
      console.log(this.param.Pengelolahinfor);
      console.log(this.param.RejectReason);
    }
    else { 
  
      this.param.IDobservasion = this.IDobservasion;

    
   console.log('Email '+this.param.PICEmail);
   console.log(this.param);
    this.global.showLoader();
    
    try {
      this.ObservasiProvider.update(this.param).subscribe( ress =>
        {
          this.global.hideLoader();
          this.router.navigate(['/home']);
          console.log(ress);
        }
      );
  } catch (error) {
      this.global.presentAlert('Error','Set PIC Data Gagal',' — Error is handled gracefully: '+ error.name);
    }
    
  
    }

     

    }


    cancel(): void{
      this.router.navigateByUrl('/home');
    }

}
