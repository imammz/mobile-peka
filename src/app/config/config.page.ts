import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-config',
  templateUrl: './config.page.html',
  styleUrls: ['./config.page.scss'],
})
export class ConfigPage implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {    
    localStorage.setItem('login', 'true');
      this.router.navigateByUrl('/home');
  }

}
