import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReviewTlPageRoutingModule } from './review-tl-routing.module';

import { ReviewTlPage } from './review-tl.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReviewTlPageRoutingModule
  ],
  declarations: [ReviewTlPage]
})
export class ReviewTlPageModule {}
