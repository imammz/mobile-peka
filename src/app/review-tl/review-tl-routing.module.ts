import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReviewTlPage } from './review-tl.page';

const routes: Routes = [
  {
    path: '',
    component: ReviewTlPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReviewTlPageRoutingModule {}
