import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditObsPage } from './edit-obs.page';

const routes: Routes = [
  {
    path: '',
    component: EditObsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditObsPageRoutingModule {}
