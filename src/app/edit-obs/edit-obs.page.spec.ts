import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditObsPage } from './edit-obs.page';

describe('EditObsPage', () => {
  let component: EditObsPage;
  let fixture: ComponentFixture<EditObsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditObsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditObsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
