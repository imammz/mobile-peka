import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditObsPageRoutingModule } from './edit-obs-routing.module';

import { EditObsPage } from './edit-obs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditObsPageRoutingModule
  ],
  declarations: [EditObsPage]
})
export class EditObsPageModule {}
