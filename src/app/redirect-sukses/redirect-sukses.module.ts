import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RedirectSuksesPageRoutingModule } from './redirect-sukses-routing.module';

import { RedirectSuksesPage } from './redirect-sukses.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RedirectSuksesPageRoutingModule
  ],
  declarations: [RedirectSuksesPage]
})
export class RedirectSuksesPageModule {}
