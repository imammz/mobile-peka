import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RedirectSuksesPage } from './redirect-sukses.page';

describe('RedirectSuksesPage', () => {
  let component: RedirectSuksesPage;
  let fixture: ComponentFixture<RedirectSuksesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedirectSuksesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RedirectSuksesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
