import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RedirectSuksesPage } from './redirect-sukses.page';

const routes: Routes = [
  {
    path: '',
    component: RedirectSuksesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RedirectSuksesPageRoutingModule {}
