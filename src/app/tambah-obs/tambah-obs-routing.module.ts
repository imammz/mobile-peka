import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TambahObsPage } from './tambah-obs.page';

const routes: Routes = [
  {
    path: '',
    component: TambahObsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TambahObsPageRoutingModule {}
