import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TambahObsPage } from './tambah-obs.page';

describe('TambahObsPage', () => {
  let component: TambahObsPage;
  let fixture: ComponentFixture<TambahObsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TambahObsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TambahObsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
