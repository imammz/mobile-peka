import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TambahObsPageRoutingModule } from './tambah-obs-routing.module';

import { TambahObsPage } from './tambah-obs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TambahObsPageRoutingModule
  ],
  declarations: [TambahObsPage]
})
export class TambahObsPageModule {}
