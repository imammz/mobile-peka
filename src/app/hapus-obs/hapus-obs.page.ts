import { Component, OnInit } from '@angular/core';
import { ObservasiService } from '../services/observasi.service';
import { Storage } from '@ionic/storage';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-hapus-obs',
  templateUrl: './hapus-obs.page.html',
  styleUrls: ['./hapus-obs.page.scss'],
})
export class HapusObsPage implements OnInit {

  
  param = {
    IDklasifikasi:'',
    spesifikasi:'',
    DateObs:'',
    Pengamatan:'',
    Lanjutan:'',
    Klasifikasi:'',
    FilePhoto:'',
    IDNIK:'',
    NamaEmploye:'',
    FungsiName:'',
    Email:'',
    NoTlp:'',
    CreateID:'',
    AreaID:'',
    jabatan:'',
    processApl:0,
    langsung:'',
    unsafeDetailId:'',
    CostCenter:'',
    UnsafeID:'',
    lokasi_tempat:'',
    processApl_desc: '',
    IDobservasion: '',
    IsActive: null,
    CreateDate: '',
    PICNIK: null,
    PICSign: null,
    PISignDate: '',
    PICEmail: null,
    PICInformasi: null,
    RiskA: null,
    RiskB: null,
    RejectReason: null,
    Pengelolahinfor: null,
    UserBypass: null,
    BypassDate: '',
    Aksi: null,
    AksiDate: '',
    AksiComment: null,
  };
  IDobservasion = '';
  userData: any = [];
  unsafe=[];
  pegawai=[];
  


  constructor(private ObservasiProvider: ObservasiService,
    private storage: Storage,
    private router: Router,
    private routeAct: ActivatedRoute,
    public global: GlobalService) { }

  ngOnInit() {
    this.IDobservasion = this.routeAct.snapshot.paramMap.get("id");
    console.log(this.IDobservasion);

    this.storage.get('userData').then((val) => {
      this.userData = val;
    });

    this.getObsById();

    this.listUnsafe();
    this.listEmploye();


  }


  getObsById(): void { 
    this.global.showLoader();
    var data = {IDobservasion: this.IDobservasion};
    
    this.ObservasiProvider.get(data).subscribe( ress =>
        {
            this.param = ress.data[0];
            this.global.hideLoader();
            console.log(this.param); 

        }
      );
  }


  delObsById(): void { 
    this.global.showLoader();
    var data = {IDobservasion: this.IDobservasion};
    
    this.ObservasiProvider.delete(data).subscribe( ress =>
        {
            this.param = ress.data[0];
            this.global.hideLoader();
            console.log(this.param); 
            this.router.navigate(['/home']);

        }
      );
  }

   
  listUnsafe(): void { 
    // this.global.showLoader();
 
     this.ObservasiProvider.listUnsafe().subscribe( ress =>
         {
             this.unsafe = ress.data;
             
            //  this.global.hideLoader();
             console.log(this.unsafe);
         }
       );
   }
 
 

   listEmploye(): void { 
    // this.global.showLoader();
     this.ObservasiProvider.listEmployee().subscribe( ress =>
         {
             this.pegawai = ress.data;
            //  this.global.hideLoader();
             console.log(this.pegawai);
         }
       );
   }
 

}
