import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HapusObsPageRoutingModule } from './hapus-obs-routing.module';

import { HapusObsPage } from './hapus-obs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HapusObsPageRoutingModule
  ],
  declarations: [HapusObsPage]
})
export class HapusObsPageModule {}
