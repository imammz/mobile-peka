import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HapusObsPage } from './hapus-obs.page';

describe('HapusObsPage', () => {
  let component: HapusObsPage;
  let fixture: ComponentFixture<HapusObsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HapusObsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HapusObsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
