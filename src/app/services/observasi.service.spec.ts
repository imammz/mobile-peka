import { TestBed } from '@angular/core/testing';

import { ObservasiService } from './observasi.service';

describe('ObservasiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ObservasiService = TestBed.get(ObservasiService);
    expect(service).toBeTruthy();
  });
});
