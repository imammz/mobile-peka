import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalService } from './global.service';


const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Headers':'Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Methods',
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Methods':'POST, GET, OPTIONS, PUT',
  'Content-Type':'application/json'
})
};


const httpOptionsForm = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Headers':'Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Methods',
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Methods':'POST, GET, OPTIONS, PUT',
   'enctype': 'multipart/form-data; boundary=----WebKitFormBoundaryuL67FWkv1CA'
})
};

@Injectable({
  providedIn: 'root'
})
export class ObservasiService {

  constructor(private http: HttpClient,
    public global: GlobalService) { }



  login(data): Observable<any> {
    /*
    PARAM:
      UserName
      Password
    */

   try {
    return this.http.post(this.global.apiUrl+'PekaEmployee/get', data, httpOptions);

  } catch (error) {
    console.log(error.status);
    console.log(error.error); // Error message as string
    console.log(error.headers);
  }

   
  }

  list(data): Observable<any> {
    /*
    PARAM:
      UserName
      filter
      costcenter
    */

   try {
    return this.http.post(this.global.apiUrl+'TObservasi/list', data, httpOptions);

  } catch (error) {
    console.log(error.status);
    console.log(error.error); // Error message as string
    console.log(error.headers);
  }

   
  }


  get(data): Observable<any> {
    /*
    PARAM:
      IDobservasion
    */
   

    try {
      return this.http.post(this.global.apiUrl+'TObservasi/get'
    , data, httpOptions);
  
    } catch (error) {
      console.log(error.status);
      console.log(error.error); // Error message as string
      console.log(error.headers);
    }
  }

  delete(data): Observable<any> {
    /*
    PARAM:
      IDobservasion
    */
    

    try {
      return this.http.post(this.global.apiUrl+'TObservasi/delete'
      , data, httpOptions);
  
    } catch (error) {
      console.log(error.status);
      console.log(error.error); // Error message as string
      console.log(error.headers);
    }
  }




  insert(data): Observable<any> {
    
    try {
      return this.http.post(this.global.apiUrl+'TObservasi/insert'
    , data);
  
    } catch (error) {
      console.log(error.status);
      console.log(error.error); // Error message as string
      console.log(error.headers);
    }
  }

  

  update(data): Observable<any> {
  
    try {
      return this.http.post(this.global.apiUrl+'TObservasi/update'
      , data);
  
    } catch (error) {
      console.log(error.status);
      console.log(error.error); // Error message as string
      console.log(error.headers);
    }
  }



  listArea(): Observable<any> {
    

    try {
      return this.http.post(this.global.apiUrl+'TArea/list_area'
      , [], httpOptions);
  
    } catch (error) {
      console.log(error.status);
      console.log(error.error); // Error message as string
      console.log(error.headers);
    }
  }
  
  listSubArea(data): Observable<any> {
    /*param
      KlarifikasiID
    */
   

    try {
      return this.http.post(this.global.apiUrl+'TSubArea/list_sub_area'
    , data, httpOptions);
  
    } catch (error) {
      console.log(error.status);
      console.log(error.error); // Error message as string
      console.log(error.headers);
    }
  }
  listUnsafe(): Observable<any> {
    /*param
    */
   

    try {
      return this.http.post(this.global.apiUrl+'TUnsafe/list'
      , [], httpOptions);
  
    } catch (error) {
      console.log(error.status);
      console.log(error.error); // Error message as string
      console.log(error.headers);
    }
  }

  listUnsDetail(data): Observable<any> {
    /*param
      UnsafeID
    */
   

    try {
      return this.http.post(this.global.apiUrl+'TUnsDetail/list'
      , data, httpOptions);
  
    } catch (error) {
      console.log(error.status);
      console.log(error.error); // Error message as string
      console.log(error.headers);
    }
  }
  
  listEmployee(): Observable<any> {
    /*param
    */
   
    try {
      return this.http.post(this.global.apiUrl+'PekaEmployee/list'
    , [], httpOptions);
  
    } catch (error) {
      console.log(error.status);
      console.log(error.error); // Error message as string
      console.log(error.headers);
    }
  }

  
  listPostCenter(): Observable<any> {
    /*param
    */
  
    try {
      return this.http.post(this.global.apiUrl+'PekaCostCenter/list'
      , [], httpOptions);
  
    } catch (error) {
      console.log(error.status);
      console.log(error.error); // Error message as string
      console.log(error.headers);
    }
  }


  
}
