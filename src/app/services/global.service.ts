import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { LoadingController, AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  constructor(public loadingController: LoadingController,
    private storage: Storage,
    private router: Router,
    private alertController: AlertController) { }


//     apiUrl = 'https://apps.pertamina.com/api/mobile_peka/public/api/';
     apiUrl = 'https://apps.pertamina.com/api/mobile_peka_prod/public/api/';
   

    public showLoader() {
     this.loadingController.create({
        message: 'Sedang Proses'
      }).then((res) => {
        res.present();

        console.log(this.loadingController);

        setTimeout(() => { 
          this.loadingController.dismiss();
        }, 15000);


      });
    }



    public showLoaderShort() {
      this.loadingController.create({
         message: 'Sedang Proses'
       }).then((res) => {
         res.present();
 
         console.log(this.loadingController);
 
         setTimeout(() => { 
           this.loadingController.dismiss();
         }, 6000);
 
 
       });
     }


    public showLoaderLong() {
      this.loadingController.create({
         message: 'Sedang Proses'
       }).then((res) => {
         res.present();
 
         console.log(this.loadingController);
 
         setTimeout(() => { 
           this.loadingController.dismiss();
         }, 48000);
 
 
       });
     }


     public showLoaderLong2() {
      this.loadingController.create({
         message: 'Sedang Proses'
       }).then((res) => {
         res.present();
 
         console.log(this.loadingController);
 
         setTimeout(() => { 
           this.loadingController.dismiss();
         }, 78000);
 
 
       });
     }



     public showLoaderNonStop() {
      this.loadingController.create({
         message: 'Sedang Proses'
       }).then((res) => {
         res.present();
 
         console.log(this.loadingController);
 
 
 
       });
     }
  
    public hideLoader() {
      console.log(this.loadingController);
      this.loadingController.dismiss();
    }


    public gotoById(url,id): void {
      this.router.navigate([url,id]);
    }

    public gotoByTwo(url,id,id2): void {
      this.router.navigate([url,id,id2]);
    }

    public goto(url): void {
      this.router.navigate([url]);
    }


    public async presentAlert(header,sub,message) {
      const alert = await this.alertController.create({
        header: header,
        subHeader: sub,
        message: message,
        buttons: ['OK']
      });
  
      await alert.present();
    }


    public debug(obj) {
      console.log(JSON.stringify(obj));
    }

  
}
